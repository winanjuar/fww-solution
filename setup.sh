#!/bin/bash

for submodule in booking-engine core-engine member-system regulation-system booking-engine
do
  echo "running npm install in ${submodule}"
  cd $submodule
  npm install --legacy-peer-deps
  cd ..
done
