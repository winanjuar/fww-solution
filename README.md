# FWW Solution
## Deskripsi
FWW Solution merupakan sistem yang digunakan oleh salah satu maskapai penerbangan, yakni FWW Airline yang memberikan benefit kepada calon penumpang berupa diskon ketika calon penumpang melakukan reservasi tiket, baik direct maupun via channel terdaftar.

Penentuan besarnya diskon yang akan diterima oleh calon penumpang terdapat dua jenis, yaitu diskon karena kota tujuan maupun diskon karena kecekatan pembayaran.

Secara sistem, FWW Solution merupakan sistem yang terintegrasi dengan service lain di luar, seperti untuk kebutuhan regulasi penerbangan yang sudah ditetapkan oleh pemerintah, yakni melakukan pengecekan status vaksin, kesesuaian data dengan disdukcapil maupun pengecekan larangan terbang dari bagian imigrasi. Selain itu FWW Solution juga menerapkan rules engine dan workflow engine yang terpisah dari main enginenya (core system maupun integration system).


## Asumsi
1. Penerbangan hanya melayani domestik NKRI dan penumpang adalah WNI yang menggunakan KTP sebagai kartu identitas penduduk utama.
2. Sistem disdukcapil adalah sistem external yang tidak dimanage oleh FWW Airline. Proses pengecekan dengan disdukcapil perlu dilakukan untuk melihat data penumpang valid atau tidak. Default: valid.
3. Sistem pedulilindungi adalah sistem external yang tidak dimanage oleh FWW Airline. Proses pengecekan dengan pedulilindungi perlu dilakukan untuk melihat status vaksin covid booster penumpang. Default: sudah booster.
4. Sistem imigrasi adalah sistem external yang tidak dimanage oleh FWW Airline. Proses pengecekan dengan imigrasi perlu dilakukan untuk melihat apakah penumpang sedang dicekal terbang atau tidak. Default: tidak.

## Desain
### Arsitektur
Sistem arsitektur
![FWW Solution](https://gitlab.com/winanjuar/fww-solution/-/raw/main/documentasi/arsitektur.png)

Komunikasi antar service
![FWW Communication](https://gitlab.com/winanjuar/fww-solution/-/raw/main/documentasi/komunikasi.png)

### ERDiagram
Berikut adalah rancangan DB yang ada di FWW Core Engine.

```mermaid
erDiagram
Airport {
  int id PK
  varchar code
  varchar name
  varchar city
  varchar timezone
}

Airplane {
  int id PK
  varchar name
  varchar registration_number
  smallint max_passenger
  smallint max_business
  smallint max_economy
  varchar chair_config
}

Seat {
  int id PK
  varchar code
  varchar seat_class
  varchar side
  varchar position
  int airplane FK
}

Seat }o--|| Airplane : belongsTo

Baggage {
  int id PK
  smallint capacity
  varchar category
  int price
}

Flight {
  int id PK
  varchar code
  int departure FK
  int destination FK
  int airplane FK
  time departure_time
  time arrival_time
  int duration
}

Price {
  int id PK
  int flight FK
  varchar seat_class
  int price_config
}

Flight }o--o{ Baggage : has
Flight }o--|| Airplane : using
Flight }o--|| Airport : flightFrom
Flight }o--|| Airport : flightTo
Flight ||--o{ Price : has

Passenger {
  varchar identity_number PK
  varchar name
  date birth_date
}

Reservation {
  int id PK
  varchar passenger FK
  int member
  varchar email
  varchar phone
  date flight_date
  int flight FK
  int seat FK
  int price_actual
  varchar booking_code
  varchar reservation_code
  varchar ticket_number
  varchar promotion_code
  varchar current_status
  varchar partner
}

Passenger ||--o{ Reservation : makes
Reservation }o--|| Seat : choose
Reservation }o--|| Flight : choose

ReservationJourney {
  int id PK
  int reservation FK
  varchar description
  datetime journey_time
}
ReservationJourney }o--|| Reservation : belongsTo

Payment {
  varchar id PK
  int reservation FK
  varchar payment_method
  int payment_final
  varchar payment_status
  datetime charge_time
  datetime check_time
  datetime payment_time
}

Payment }o--|| Reservation : belongsTo

PaymentDetail {
  varchar id PK
  varchar payment FK
  varchar name
  int quantity
  intn price
}

Payment ||--o{ PaymentDetail : has

```

Berikut adalah rancangan DB yang ada di FWW Booking Engine.

```mermaid
erDiagram
SeatStatus {
  int id PK
  varchar passenger
  date flight_date
  int flight
  int departure
  int destination
  int airplane
  int seat
  varchar seat_class
  varchar seat_number
  int price
  varchar status
}

Partner {
  int id PK
  varchar cognito_id
  varchar name
  varchar username
  varchar pic_email
  varchar pic_phone
}

Workflow {
  varchar process_instance_id PK
  int reservation_id
}

WorkflowDetail {
  varchar task_id PK
  varchar workflow FK
  varchar activity_name
}

Workflow ||--o{ WorkflowDetail : has
```


## Instalasi
### Running as multiple service
```bash
# lakukan `git clone` pada metarepo fww-solution
$ git clone https://gitlab.com/winanjuar/fww-solution.git
```

```bash
# fetch submodule
$ git submodule update --init --recursive
```

```bash
# npm install di setiap submodule dengan shell script sederhana yang sudah disiapkan
$ chmod u+x setup.sh
$ ./setup.sh
```
```bash
Load dump data yang sudah disiapkan ke database sesuai environment
Deploy BPM ke Camunda Server
```

```bash
# pastikan semua environment pendukung (camunda, mongodb, mariadb, redis, rabbitmq) sudah running, kemudian jalankan perintah berikut di masing-masing service (environment disesuaikan dengan config masing masing)
$ npm run start
```
### Running as multiple container docker
```bash
# lakukan `git clone` pada metarepo fww-solution
$ git clone https://gitlab.com/winanjuar/fww-solution.git
```

```bash
# fetch submodule
$ git submodule update --init --recursive
```

```bash
Load dump data yang sudah disiapkan ke database sesuai environment
Deploy BPM ke Camunda Server
```

```bash
# lakukan build dan running container di setiap service dengan menggunakan perintah
$ make build
$ make run cport=`portservice`
```
### Running as bundle container docker compose
```bash
# lakukan `git clone` pada metarepo fww-solution
$ git clone https://gitlab.com/winanjuar/fww-solution.git
```
```bash
# fetch submodule
$ git submodule update --init --recursive
```

```bash
# build image and run container
$ docker-compose -f docker-compose.yml --env-file=`yourcomposeenvfile`.env up -d
```

```bash
# load data seeder ke database mariadb
$ docker exec -i mariadb mysql -u`yourmariadbuser` -p`yourmariadbpassword` core_engine_db < ./sql/core-engine.sql
$ docker exec -i mariadb mysql -u`yourmariadbuser` -p`yourmariadbpassword` booking_engine_db < ./sql/booking-engine.sql
$ docker exec -i mariadb mysql -u`yourmariadbuser` -p`yourmariadbpassword` member_db < ./sql/member.sql
$ docker exec -i mariadb mysql -u`yourmariadbuser` -p`yourmariadbpassword` regulation_db < ./sql/regulation.sql
```

```bash
# load data seeder ke database mongodb
$ docker cp ./mongodb-init.json mongodb:/tmp/mongodb-init.json
$ docker exec -i mongodb mongoimport --host mongodb --db fwwrule --collection instructions --type json --file /tmp/mongodb-init.json --jsonArray
```

```bash
Deploy BPM ke Camunda Server
```

```bash
# jika sudah selesai jangan lupa untuk dimatikan kembali dengan perintah
$ docker-compose -f docker-compose.yml --env-file=`yourcomposeenvfile`.env down
```
Contoh container yang sedang running

![Running Container](https://gitlab.com/winanjuar/fww-solution/-/raw/main/documentasi/container.png)

